export function normalizeNumber(
    num: number | string, errorIfNotNumber: string) : number {
    if (typeof num === 'undefined') {
    throw new Error(`${errorIfNotNumber} -- ${num}`);
    }
    if (typeof num === 'number') return num;
    let ret = parseInt(num);
    if (isNaN(ret)) {
    throw new Error(`${errorIfNotNumber} ${ret} -- ${num}`);
    }
    return ret!;
}