import { UserController } from './controller/user.controller';
import * as express from 'express';
import "reflect-metadata";
import { createConnection } from "typeorm";

class Server {
  private app: express.Application;
  private userController: UserController;

  constructor(){
    this.app = express(); // init the application
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.configuration();
    this.routes();
  }

  /**
   * Used to start the server
   */
     public start(){
      this.app.listen(this.app.get('port'), () => {
        console.log(`Express is listening at http://localhost:${this.app.get('port')}`);
      });
    }

  /**
   * Method to configure the server,
   * If we didn't configure the port into the environment 
   * variables it takes the default port 3000
   */
  public configuration() {
    this.app.set('port', process.env.PORT || 3000);
    this.app.use(express.json());
    // typeORM by default looks for ormconfig.json file, that is why we don't provide any config here
  // more on it: https://orkhan.gitbook.io/typeorm/docs/using-ormconfig
    const connection = createConnection().then(connection => {
      console.log("Successfully connected to DB")
    }).catch(error => console.log(error));
  }

  /**
   * Method to configure the routes
   */
   public async routes(){
      this.userController = new UserController();
      this.app.use('/users/', this.userController.router);
   }
}

const server = new Server(); // Create server instance
server.start(); // Execute the server