import { User } from './User';
import { 
    Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn
} from "typeorm";

@Entity({ name: "Accounts" })
export class Account {
    @PrimaryGeneratedColumn("uuid")  id: number;
    @Column({type: "varchar", length: 50})  email: string;
    @Column({type: "varchar", length: 50})  password: string;
    @OneToOne(() => User, user => user.id)
    @JoinColumn() user: User;
}