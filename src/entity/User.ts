import { 
    Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn
} from "typeorm";
import { Account } from "./Account";

@Entity({ name: "Users" })
export class User {
    @PrimaryGeneratedColumn("uuid")  id: number;
    @Column({type: "varchar", length: 50}) email: string;
    @Column({type: "varchar", length: 25}) firstName: string;
    @Column({type: "varchar", length: 25}) lastName: string;
}