import { 
    EntityRepository, Repository, getRepository 
} from "typeorm";
import { User } from "../entity/User";
import * as util from 'util';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async createAndSave(user: User): Promise<User> {
        try{
            // should figure out why we have to use getRepository(User) instead of "this.save(user)" since we already are in the repo
            // documentation at: https://typeorm.io/#/custom-repository
            await getRepository(User).save(user);
        } catch(err) {
            console.log(err)
        }
        console.log("New User Saved", user);
        return user;
    }

    async allUsers(): Promise<User []> {
        let users = await getRepository(User).find();
        return users;
    }

    async findOneUser(id: number):
                Promise<User> {
        let user = await this.findOne({ 
            where: { id: id }
        });
        if (!UserRepository.isUser(user)) {
            throw new Error(`User id ${util.inspect(id)} did not retrieve a User`);
        }
        return user;
    }

    async deleteUser(user: number | User) {
        if (typeof user !== 'number'
         && !UserRepository.isUser(user)) {
            throw new Error('Supplied user object not a user');
        }
        await this.manager.delete(User, 
                typeof user === 'number' 
                    ? user : user.id);
    }


    //typescript way of runtime check
    static isUser(user: any): user is User {
    return typeof user === 'object'
        && typeof user.firstName === 'string'
        && typeof user.lastName === 'string'
        && typeof user.email === 'string'
        && typeof user.id === 'number'
    }

}

