import {
    EntityRepository, 
    getRepository, 
    Repository, 
} from "typeorm";
import { 
    Account 
} from '../entity/Account';
import * as util from 'util';


@EntityRepository(Account)
export class AccountRepository extends Repository<Account> {
    async createAndSave(account: Account): Promise<Account> {
        try{
            await getRepository(Account).save(account);
        } catch(err) {
            console.log(err)
        }
        console.log("New account Saved", account);
        return account;
    }

    async allAccounts(): Promise<Account []> {
        let accounts = await this.find();
        return accounts;
    }

    async findOneAccount(id: number):
                Promise<Account> {
        let account = await this.findOne({ 
            where: { id: id }
        });
        if (!AccountRepository.isAccount(account)) {
            throw new Error(`Account id ${util.inspect(id)} did not retrieve an Account`);
        }
        return account;
    }

    async deleteAccount(account: number | Account) {
        if (typeof account !== 'number'
         && !AccountRepository.isAccount(account)) {
            throw new Error('Supplied account object not a account');
        }
        await this.manager.delete(Account, 
                typeof account === 'number' 
                    ? account : account.id);
    }


    //typescript way of runtime check
    static isAccount(account: any): account is Account {
    return typeof account === 'object'
        && typeof account.password === 'string'
        && typeof account.email === 'string'
        && typeof account.id === 'number'
        && typeof account.userId === 'number'
    }
    


}