import { AccountRepository } from './../repository/AccountRepository';
import { Request, Response, Router } from 'express';
import { UserRepository } from '../repository/UserRepository';
import { User } from '../entity/User';
import { Account } from '../entity/Account';

// https://javascript.plainenglish.io/create-a-rest-api-with-express-postgresql-typeorm-and-typescript-ac42a20b66c7
// usersRouter.get("/", (req, res) => {
//   console.log();
//   return res.json(userRepo.allUsers());
// });

export class UserController {
  public router: Router;
  private userRepository: UserRepository; 
  private accountRepository: AccountRepository;

  constructor(){
    this.userRepository = new UserRepository(); // Create a new instance of UserRepository
    this.accountRepository = new AccountRepository(); // Create a new instance of UserRepository
    this.router = Router();
    this.routes();
  }

  // finds all users
  public getUsers = async (req: Request, res: Response) => {
    const users = await this.userRepository.allUsers();
    res.send(users).json();
  } 

  //Creates account and user and returns the account
  public createAccount = async (req: Request, res: Response) => {
    const user = new User();
    user.firstName = req.body.firstName
    user.lastName = req.body.lastName
    user.email = req.body.email
    const newUser = await this.userRepository.createAndSave(user);
    const account = new Account();
    account.email = req.body.email
    account.password = req.body.password
    account.user = newUser
    const newAccount = await this.accountRepository.createAndSave(account);
    res.send(newAccount).json();
  } 

  /**
 * Configure the routes of controller
 */
  public routes(){
    this.router.get('/', this.getUsers);
    this.router.post('/createAccount', this.createAccount);
    // this.router.put('/:id', this.update);
    // this.router.delete('/:id', this.delete);
  }
}
